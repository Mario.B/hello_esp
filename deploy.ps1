function esp([parameter(ValueFromRemainingArguments=$true)]$Args)
{
	echo "ampy -p COM3 $Args"
	ampy -p COM3 $Args
}

Get-ChildItem . -Filter *.py | ForEach-Object {esp put $_}
