import machine
import time

class LedController:
  yellow_led_pin_number = 17
  green_led_pin_number = 25
  red_led_pin_number = 32
  white_led_pin_number = 33

  def __init__(self):
    self.led_green = machine.Pin(LedController.green_led_pin_number, machine.Pin.OUT)
    self.led_red = machine.Pin(LedController.red_led_pin_number, machine.Pin.OUT)
    self.led_yellow = machine.Pin(LedController.yellow_led_pin_number, machine.Pin.OUT)
    self.led_white = machine.Pin(LedController.white_led_pin_number, machine.Pin.OUT)

    self.leds_list = [self.led_white, self.led_red, self.led_yellow, self.led_green]

    self.__is_running = False

  def all_off(self):
    for led in self.leds_list:
      led.value(0)

  def all_on(self):
    for led in self.leds_list:
      led.value(1)

  # ----------- <old_code>
  def run_animation(self):
    self.all_off()

    while True:
      if self.__is_running:
        for led in self.leds_list:
          led.value(1)
          time.sleep_ms(100)

        for led in self.leds_list:
          led.value(0)
          time.sleep_ms(100)
      else:
        time.sleep_ms(500)

  def request_stop_animation(self):
    self.__is_running = False

  def request_start_animation(self):
    self.__is_running = True

  # ------------- </old_code>