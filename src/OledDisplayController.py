# Complete project details at https://RandomNerdTutorials.com

from machine import Pin, I2C
import ssd1306
from time import sleep

class OledDisplayController:
  def __init__(self, width = 128, height = 64):
    # ESP32 Pin assignment
    i2c = I2C(-1, scl=Pin(22), sda=Pin(21))

    # ESP8266 Pin assignment
    #i2c = I2C(-1, scl=Pin(5), sda=Pin(4))

    self.oled_width = width
    self.oled_height = height

    self.oled = ssd1306.SSD1306_I2C(self.oled_width, self.oled_height, i2c)

  def demo(self):
    self.oled.text('Hello, World 1!', 0, 0)
    self.oled.text('Hello, World 2!', 0, 10)
    self.oled.text('Hello, World 3!', 0, 20)

    self.oled.show()

  def all_off(self):
    self.oled.fill(0)
    self.oled.show()

  def all_on(self):
    self.oled.fill(1)
    self.oled.show()
