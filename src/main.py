import utils
import LedController
import ButtonsController
import OledDisplayController
import network

class Program:
  def __init__(self):
    self.led_controller = self.__initialize_leds()
    self.wlan = self.__initialize_wlan()
    self.button_controller = self.__initialize_buttons()
    self.oled_controller =self.__initialize_oled_controller()

  def __initialize_leds(self):
    led_controller = LedController.LedController()
    led_controller.all_off()
    return led_controller

  def __initialize_wlan(self):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.scan()
    wlan.connect('', '')
    return wlan

  def __initialize_buttons(self):
    return ButtonsController.ButtonsController()

  def __initialize_oled_controller(self):
    oled = OledDisplayController.OledDisplayController()
    oled.all_off()
    return oled

  def print_button_states(self):
    self.oled_controller.oled.fill(0)
    self.oled_controller.oled.text("Button 1: {}".format(self.button_controller.button_1.value()), 0, 0)
    self.oled_controller.oled.text("Button 2: {}".format(self.button_controller.button_2.value()), 0, 10)
    self.oled_controller.oled.show()

  def start(self):
    self.button_controller.on_button_1_change(self.print_button_states)
    self.button_controller.on_button_2_change(self.print_button_states)
    #self.led_controller.run()


def main():
  program = Program()
  print("WLAN configuration:")
  print(program.wlan.ifconfig())
  program.start()

if __name__ == "__main__":
  main()
