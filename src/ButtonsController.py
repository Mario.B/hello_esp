from machine import Pin

class ButtonsController:
  button_1_pin_number = 16
  button_2_pin_number = 34

  def __init__(self):
    self.button_1 = Pin(ButtonsController.button_1_pin_number, Pin.IN)
    self.button_2 = Pin(ButtonsController.button_2_pin_number, Pin.IN)


  def on_button_1_change(self, callback, trigger = Pin.IRQ_FALLING | Pin.IRQ_RISING):
    def onCallback(pin):
      print ("on change button 1 - pin: '{}'".format(pin))
      callback()

    self.button_1.irq(trigger=trigger, handler=onCallback)


  def on_button_2_change(self, callback, trigger = Pin.IRQ_FALLING | Pin.IRQ_RISING):
    def onCallback(pin):
      print ("on change button 2 - pin: '{}'".format(pin))
      callback()

    self.button_2.irq(trigger=trigger, handler=onCallback)
