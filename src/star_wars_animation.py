# http://docs.micropython.org/en/v1.10/esp8266/tutorial/network_tcp.html

import socket

def main():
  addr_info = socket.getaddrinfo("towel.blinkenlights.nl", 23)

  addr = addr_info[0][-1]
  s = socket.socket()
  s.connect(addr)

  while True:
    data = s.recv(500)
    print(str(data, 'utf8'), end='')

if __name__ == "__main__":
  main()
