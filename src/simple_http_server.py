import socket
import machine

class simple_http_server:
  @staticmethod
  def __get_pins():
    return [machine.Pin(i, machine.Pin.IN) for i in (0, 2, 4, 5, 12, 13, 14, 15)]


  @staticmethod
  def __get_html() -> str:
    return """<!DOCTYPE html>
    <html>
        <head> <title>ESP8266 Pins</title> </head>
        <body> <h1>ESP8266 Pins</h1>
            <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
        </body>
    </html>
    """


  @staticmethod
  def __initialize_socket():
    addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

    soc = socket.socket()
    soc.bind(addr)
    soc.listen(1)

    print('listening on', addr)

    return soc


  @staticmethod
  def start():
    soc = simple_http_server.__initialize_socket()
    static_html = simple_http_server.__get_html()

    while True:
      conn, addr = soc.accept()
      print('client connected from', addr)
      cl_file = conn.makefile('rwb', 0)

      while True:
        line = cl_file.readline()
        if not line or line == b'\r\n':
          break

      rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in simple_http_server.__get_pins()]
      response = static_html % '\n'.join(rows)
      conn.send(response)
      conn.close()

