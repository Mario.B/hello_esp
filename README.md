# HelloEsp

## Deployment

```powershell
cd .\src\
..\deploy.ps1
```

## Nächster Step

[Hier weitermachen mit der Doku](http://docs.micropython.org/en/v1.10/esp8266/tutorial/network_tcp.html)

## Tools

[Ampy](https://github.com/scientifichackers/ampy) is a useful tool for interacting with the micropython filesystem

[Webrepl client](http://micropython.org/webrepl/) for accessing the repl without blocking the serial port

[esptool](https://github.com/espressif/esptool)

[data sheet of my esp32](http://wiki.ai-thinker.com/_media/esp32/docs/nodemcu-32s_product_specification.pdf)

### Micropy

[Micropy documetaton link](https://pypi.org/project/micropy-cli/)

To setup a Micropy environment locally, simply:

- Install micropy-cli
- Navigate to the project directory
- Execute micropy

-----

## Useful links

- [LCD Display 'datasheet'](https://www.openhacks.com/uploadsproductos/eone-1602a1.pdf)

- [ESP8266 Tutorial](http://docs.micropython.org/en/v1.10/esp8266/tutorial/repl.html) ist für ESP32 nahezu genuso anwendbar.

- [PyLint Doku](https://docs.pylint.org/en/1.6.0/)

- [Library für das LCD Display](https://github.com/dhylands/python_lcd)

### Micropython documentation links

WLAN aktivieren:

- [API Reference](https://docs.micropython.org/en/latest/library/network.WLAN.html)
- [Beispiel](http://docs.micropython.org/en/v1.10/esp32/quickref.html#networking)

Pins ansteuern:

- [API Reference](http://docs.micropython.org/en/v1.10/library/machine.Pin.html#machine-pin)
- [Beispiel](http://docs.micropython.org/en/v1.10/esp32/quickref.html#pins-and-gpio)

## Pins

36

```python
machine.Pin(36, machine.Pin.OUT)
#pin can only be input
```
